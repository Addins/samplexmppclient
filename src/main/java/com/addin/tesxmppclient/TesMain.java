package com.addin.tesxmppclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateListener;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jxmpp.jid.FullJid;
import org.jxmpp.jid.impl.JidCreate;

/**
 *
 * @author addin
 */
public class TesMain {

    static XMPPConnection connection;
    static RosterEntry test = null;
    // TODO add list of roster here that contain jid/resource.

    public static void main(String[] args) {

        String serviceName = "addin-k43sj";
        int port = 5222;
        String userName = "admin";
        String password = "1234";
        String host = "localhost";

        try {

            SmackConfiguration.DEBUG = true;
            XMPPTCPConnectionConfiguration conf = XMPPTCPConnectionConfiguration.builder()
                    .setServiceName(JidCreate.domainBareFrom(serviceName))
                    .setHost(host)
                    .setResource(serviceName)
                    .setPort(port)
                    .setConnectTimeout(90000)
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                    .setDebuggerEnabled(true)
                    .setCompressionEnabled(true)
                    .build();

            connection = new XMPPTCPConnection(conf);

            System.out.println("Connecting...");
            try {
                ((AbstractXMPPConnection) connection).connect();

                amIConnected();
                isConnectionSecure();

                System.out.println("Logging in...");
                ((AbstractXMPPConnection) connection).login(userName, password);

            } catch (InterruptedException ex) {
                Logger.getLogger(TesMain.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (connection.isAuthenticated()) {
                System.out.println("You logged in as " + connection.getUser().asBareJidString());

                // adding several listener to connection
                connection.addAsyncStanzaListener(new PacketListener() {

                    @Override
                    public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                        Presence p = (Presence) packet;
                        System.out.println(" --presence-- " + p.getFrom().asDomainBareJidString() + " is " + p.getType().name());
                        // someone asking to subscribe your presence?
                        if (p.getType().equals(Presence.Type.subscribe) || p.getType().equals(Presence.Type.unsubscribe)) {
                            System.out.println("req0--" + p.getFrom());
                        }
                    }
                }, new PacketTypeFilter(Presence.class));

                // get roster 
                Roster roster = Roster.getInstanceFor(connection);
                roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
//                roster.addRosterLoadedListener(new RosterLoadedListener() {
//
//                    @Override
//                    public void onRosterLoaded(Roster roster) {
//                        for (RosterEntry e : roster.getEntries()) {
//                            if (e.getUser().asDomainBareJidString().contains("test")) {
//                                test = e;
//                            }
//                        }
//                    }
//                });

                // get chat manager
                ChatManager chatManager = ChatManager.getInstanceFor(connection);
                chatManager.setMatchMode(ChatManager.MatchMode.BARE_JID);

                // add listener to chat manager
                chatManager.addChatListener(new ChatManagerListener() {
                    @Override
                    public void chatCreated(Chat arg0, boolean arg1) {
                        if (!arg1) {
                            arg0.addMessageListener(new ChatStateListener() {

                                @Override
                                public void stateChanged(Chat arg0, ChatState arg1) {
                                    System.out.println(" --chat_state(1)-- " + arg0.getParticipant() + " is " + arg1.toString());
                                }

                                @Override
                                public void processMessage(Chat arg0, Message arg1) {
                                    if (arg1.getBody() != null) {
                                        System.out.println("new Message from " + arg0.getParticipant() + " >> "
                                                + arg1.getBody());
                                        try {
                                            arg0.sendMessage("I am Groot...");
                                        } catch (SmackException.NotConnectedException | InterruptedException ex) {
                                            Logger.getLogger(TesMain.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    ExtensionElement pe = arg1.getExtension("http://jabber.org/protocol/chatstates");
                                    if (pe != null) {
                                        System.out.println(" --chat_state(2)-- " + arg0.getParticipant() + " is "
                                                + pe.getElementName());
                                    }
                                }
                            });
                        }
                    }
                });

                // get file transfer manager instance
                FileTransferManager ftm = FileTransferManager.getInstanceFor(connection);

                // add file transfer listener
                ftm.addFileTransferListener(new FileTransferListener() {

                    @Override
                    public void fileTransferRequest(FileTransferRequest request) {
                        IncomingFileTransfer ift = request.accept();
                        
                        String name = request.getFileName();
                        System.out.println("filename = " + name + " from " + request.getRequestor());
                        try {
                            ift.recieveFile(new File(request.getFileName()));
                            while (!ift.isDone()) {
                                final double progress = ift.getProgress();
                                final double progressPercent = progress * 100.0;
                                String percComplete = String.format("%1$,.2f", progressPercent);
                                Logger.getLogger(TesMain.class.getName()).log(Level.INFO, "Transfer status is: {0}", ift.getStatus());
                                Logger.getLogger(TesMain.class.getName()).log(Level.INFO, "File transfer is {0}% complete", percComplete);
                            }

                        } catch (SmackException | IOException ex) {
                            Logger.getLogger(TesMain.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        System.out.println("filename = " + name + " from " + request.getRequestor() + " received.");
//                        System.out.println("err --- "+ift.getError().getMessage());
//                        System.out.println("cause --- "+ift.getException().getCause().getMessage());
                    }
                });
            }

            System.out.println("Press 'c' to quit.");
            while (connection.isConnected()) {
                BufferedReader buffInp = new BufferedReader(new InputStreamReader(System.in));
                String inp = buffInp.readLine();
                if (inp.equals("c")) {
                    break;
                }
                if (inp.equals("any")) {
                }
                if (inp.equals("ftr")) {
                    sendFile(JidCreate.fullFrom(""));
                }
            }

            System.out.println("Disconnecting...");
            ((AbstractXMPPConnection) connection).disconnect();
            amIConnected();
        } catch (SmackException | IOException | XMPPException ex) {
            Logger.getLogger(TesMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void amIConnected() {
        System.out.print("Am I connected? ");
        if (connection.isConnected()) {
            System.out.println("You are connected.");
        } else {
            System.out.println("No, you are not.");
        }
    }

    public static void isConnectionSecure() {
        System.out.print("Is the connection secure? ");
        if (connection.isSecureConnection()) {
            System.out.println("The connection is secure.");
        } else {
            System.out.println("The connection is NOT secure.");
        }
    }

    public static void sendFile(FullJid to) throws SmackException {
        // get file transfer manager instance
        FileTransferManager ftm = FileTransferManager.getInstanceFor(connection);
        System.out.println("sending file to " + to);
        OutgoingFileTransfer oft = ftm.createOutgoingFileTransfer(to);
        oft.sendFile(new File("pom.xml"), "file transfer test.");
    }
}
